Gatling 3 Fundamentals
=========================

Course code for the Gatling Fundamentals Udemy course - updated for Gatling v3
https://www.udemy.com/gatling-fundamentals


# Install

## Install and run the VideoGame App

The VideoGameDB app provides some API endpoints at which the Gatling tutorial fires Galting tests.

To clone and run the app locally ...
```
git clone git@github.com:james-willett/VideoGameDB.git
cd VideoGameDB
./gradelw bootRun
```

When the application is running, open a browser and go to http://localhost:8080/swagger-ui/index.html#/ to explore the API endpoints.

(The above is covered in [Lecture 12](https://www.udemy.com/course/gatling-fundamentals/learn/lecture/11936604#questions) of ths Udemy course.)


## Install Gatling

### Dependencies
* Java 8 JDK
* Maven
* IntelliJ IDEA
* Scala Plugin for IntelliJ

### pom.xml

```xml
 <properties>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <gatling.version>${project.version}</gatling.version>
    <gatling-plugin.version>3.0.3</gatling-plugin.version>
    <scala-maven-plugin.version>4.2.0</scala-maven-plugin.version>
  </properties>
```
```xml
  <dependencies>
    <dependency>
      <groupId>io.gatling.highcharts</groupId>
      <artifactId>gatling-charts-highcharts</artifactId>
      <version>${gatling.version}</version>
      <scope>test</scope>
    </dependency>
  </dependencies>
```

Full ```pom.xml``` file for the tutorials project can be found [here](pom.xml).

## Install a Proxy

If you want to be able to see the traffic to help you debug it, then a proxy is a good idea. To install Fiddler Everywhere follow these steps:

* 

# Learning Concepts

|Concept |Lesson Location|Code Sample Locations|Other Docs|
|---|---|---|---|
|||||
|||||
|Use JsonPath to check a response|xxx|[CheckResponseWithJsonPath.scala](/src/test/scala/simulations/CheckResponseWithJsonPath.scala)|Evaluate Json paths using the [JsonPath Online Evaluator](http://jsonpath.com/)|
|Creating and debugging session variables|[Lesson 23](https://www.udemy.com/course/gatling-fundamentals/learn/lecture/11936688#overview)|[CheckResponseWithJsonPath.scala](/src/test/scala/simulations/CheckResponseWithJsonPath.scala)|[Debug Gatling](james-willett.com/debug-gatling)|
|Extracting response body|[Lesson 23](https://www.udemy.com/course/gatling-fundamentals/learn/lecture/11936688#overview)|[CheckResponseWithJsonPath.scala](/src/test/scala/simulations/CheckResponseWithJsonPath.scala)|[Debug Gatling](james-willett.com/debug-gatling)|
|Functional decomposition / code reuse|[Lesson 24](https://www.udemy.com/course/gatling-fundamentals/learn/lecture/11936690#overview)|[CodeReuse.scala](/src/test/scala/simulations/CodeReuse.scala)|xxx|
|Repeats|[Lesson 25](https://www.udemy.com/course/gatling-fundamentals/learn/lecture/11936694#overview)|[Repeats.scala](/src/test/scala/simulations/Repeats.scala)|xxx|
|Feeder - CSV file|[Lesson 27](https://www.udemy.com/course/gatling-fundamentals/learn/lecture/11948080#overview)|[CsvFeeder.scala](/src/test/scala/simulations/CsvFeeder.scala)|xxx|
|Basic load simulation|[Lesson 33](https://www.udemy.com/course/gatling-fundamentals/learn/lecture/11948054#overview)|[BasicLoadSimulation.scala](/src/test/scala/simulations/BasicLoadSimulation.scala)|Injection Profile in [Gatling Cheat Sheet](https://gatling.io/docs/current/cheat-sheet)|
|Load of fixed duration|[Lesson 35](https://www.udemy.com/course/gatling-fundamentals/learn/lecture/11948106#overview)|[RampLoadForFixedTime.scala](/src/test/scala/simulations/RampLoadForFixedTime.scala)|Injection Profile in [Gatling Cheat Sheet](https://gatling.io/docs/current/cheat-sheet)|
|Running from the CLI|[Lesson 37](https://www.udemy.com/course/gatling-fundamentals/learn/lecture/12018742#overview)|[RampLoadForFixedTime.scala](/src/test/scala/simulations/RampLoadForFixedTime.scala)|Injection Profile in [Gatling Cheat Sheet](https://gatling.io/docs/current/cheat-sheet)|
|Using runtime parameters|[Lesson 38](https://www.udemy.com/course/gatling-fundamentals/learn/lecture/12018748#overview)|[RuntimeParameters.scala](/src/test/scala/simulations/RuntimeParameters.scala)|[gatling-maven-plugin](https://gatling.io/docs/current/extensions/maven_plugin/) & [gatling-maven-plugin-demo](https://github.com/gatling/gatling-maven-plugin-demo)|



