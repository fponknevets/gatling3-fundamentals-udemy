package simulations

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration.DurationInt

// get your json paths from https://jsonpath.com/
// learn about jsonpath at https://goessner.net/article/JsonPath


class CheckResponseWithJsonPath extends Simulation{

  // create configuration
  val httpConf = http.baseUrl("http://localhost:8080/app/")
    .header("Accept", "application/json")
    // .proxy(Proxy("localhost",8888))

  // specify scenario
  val scn1 = scenario("Checking response body with JsonPath")

    .exec(
      http("Get all games and save the second result")
        .get("videogames/")
        .check(jsonPath("$[1].id").is("3"))
    )

  val scn2 = scenario("Checking response body with JsonPath")

    .exec(
      http("Get all games and save the second result")
        .get("videogames/")
        // creating session variables
        .check(jsonPath("$[1].id").saveAs("secondGame_gameId"))
        .check(jsonPath("$[1].name").saveAs("secondGame_name")))

    // debugging session variables
    .exec { session => println(session); session}

    .exec(
      http("Get game with Id from last call")
        .get("videogames/${secondGame_gameId}")
        .check(jsonPath("$.name").is("${secondGame_name}"))
        .check(bodyString.saveAs("responseBody")))
    // debugging response body contents
    .exec { session => println(session("responseBody").as[String]); session}


  // execute scenario
  setUp(
    scn2.inject(atOnceUsers(1))
  ).protocols(httpConf)
}
