package simulations

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class MyFirstTest extends Simulation {

  // Http conf
  val httpConf = http.baseUrl("http://localhost:8080/app")
    .header("Accept", "application/json")
    //.proxy(Proxy("localhost",8888))

  // Scenario definition
  val scn = scenario("My First Test")
    .exec(http("Get All Games").get("/videogames"))

  // Load Scenario
  setUp(
    scn.inject(atOnceUsers(1))
  ).protocols(httpConf)

}
