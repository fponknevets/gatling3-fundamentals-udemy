package simulations

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class CsvFeeder extends Simulation{

  // configuration
  val httpConf = http.baseUrl("http://localhost:8080/app/")
    .header("Accept", "application/json")
    // .proxy(Proxy("localhost",8888))

  // setup the data feeder
  val csvFeeder = csv("data/gameData.csv").circular

  // functional decomposition - these could go in a separate class really
  def getVideoGameWithId() = {
    repeat(10) {
      feed(csvFeeder)
        .exec(http("Get video game by ID")
        .get("videogames/${gameId}")
        .check(jsonPath("$.name").is("${gameName}"))
        .check(status.is(200)))
        .pause(1)
    }
  }

  // specify scenario using functional decomposition and code reuse
  val scn = scenario("Calls to the API using functions")
    .exec(getVideoGameWithId())

  setUp(
    scn.inject(atOnceUsers(1))
  ).protocols(httpConf)
}
