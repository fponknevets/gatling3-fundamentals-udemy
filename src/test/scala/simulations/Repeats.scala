package simulations

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class Repeats extends Simulation{

  // configuration
  val httpConf = http.baseUrl("http://localhost:8080/app/")
    .header("Accept", "application/json")
    // .proxy(Proxy("localhost",8888))

  // functional decomposition - these could go in a separate class really
  def getAllVideoGames() = {
    repeat(3) {
      exec(http("Get all video games")
        .get("videogames")
        .check(status.is(200))
      )
    }
  }

  // functional decomposition - these could go in a separate class really
  def getVideoGameWithId(gameId : String) = {
    repeat(5) {
      exec(http("Get video game by ID")
        .get("videogames/" + gameId)
        .check(status.is(200))
      )
    }
  }

  // specify scenario using functional decomposition and code reuse
  val scn = scenario("Calls to the API using functions")
    .exec(getAllVideoGames())
    .exec(getVideoGameWithId("3"))
    .exec(getAllVideoGames())

  setUp(
    scn.inject(atOnceUsers(1))
  ).protocols(httpConf)
}
