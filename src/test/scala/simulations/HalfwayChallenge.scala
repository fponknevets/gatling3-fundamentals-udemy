
// call this script from the command line with
// mvn gatling:test -Dgatling.simulationClass=simulations.RuntimeParameters -DUSERS=10 -DRAMP_DURATION=5 -DTEST_DURATION=20
// the command line parameter values will override the default values supplied to the getProperty method

package simulations

// imports
import java.time.LocalDate
import java.time.format.DateTimeFormatter

import scala.util.Random
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class HalfwayChallenge extends Simulation{

  // configuration
  val httpConf = http.baseUrl("http://localhost:8080/app/")
    .header("Accept", "application/json")

  // VARIABLES
  // runtime variables
  def userCount:Int = getProperty("USERS", "5").toInt
  def rampDuration:Int = getProperty("RAMP_DURATION", "5").toInt
  def testDuration:Int = getProperty("TEST_DURATION", "10").toInt
  //other variables
  val idNumber = (1010 to 2000).iterator
  val rnd = new Random()
  val now = LocalDate.now()
  val pattern = DateTimeFormatter.ofPattern("yyy-MM-dd")

  // HELPER METHODS
  def getProperty(propertyName:String, defaultPropertyValue:String) = {
    Option(
      System.getenv(propertyName))
      .orElse(Option(System.getProperty(propertyName)))
      .getOrElse(defaultPropertyValue)
  }

  def getRandomStringOfLength(length:Int) = {
    rnd.alphanumeric.filter(_.isLetter).take(length).mkString
  }

  def getRandomDate(startDate: LocalDate, random: Random) = {
    startDate.minusDays(random.nextInt(30)).format(pattern)
  }

  // custom feeder
  val randomGameFeeder = Iterator.continually(
    Map (
      "gameId" -> idNumber.next(),
      "name" -> ("Game-" + getRandomStringOfLength(5)),
      "releaseDate" -> getRandomDate(now, rnd),
      "reviewScore" -> rnd.nextInt(100),
      "category" -> ("Category-" + getRandomStringOfLength(6)),
      "rating" -> ("Rating-" + getRandomStringOfLength(4))
    )
  )

  // http calls
  def getAllGames() = {
    exec(http("get all video games")
      .get("videogames")
      .check(status.is(200))
    )
  }

  def createGameWithFeederAndTemplate() = {
    feed(randomGameFeeder)
      .exec(http("create game")
        .post("videogames")
        .body(ElFileBody("templates/Game.json")).asJson
        .check(status.is(200)))
      .pause(1)
  }

  def checkNameOfGameWithId(id:String) = {
    exec(http("check name of game with id")
      .get("videogames/${gameId}")
      .check(jsonPath("$.name").is("${name}"))
      .check(status.is(200))
    )
  }

  def deleteGameWithID(id:String) = {
    exec(http("delete game with id")
      .delete("videogames/${gameId}")
      .check(status.is(200))
    )
  }

  // scenario
  val scn = scenario("create random game")
    .exec(getAllGames())
    .pause(1)
    .exec(createGameWithFeederAndTemplate())
    .pause(1)
    .exec(checkNameOfGameWithId("${gameId}"))
    .pause(1)
    .exec(deleteGameWithID("${gameId}"))

  before(
    println("Before")
  )

  after(
    print("After")
  )

  // implement
  setUp(
    scn.inject(
      nothingFor(5 seconds),
      rampUsers(userCount) during(rampDuration seconds)
    )
  ).protocols(httpConf).maxDuration(testDuration)


  // helper functions



}
