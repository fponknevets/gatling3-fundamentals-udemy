
// call this script from the command line with
// mvn gatling:test -Dgatling.simulationClass=simulations.RuntimeParameters -DUSERS=10 -DRAMP_DURATION=5 -DTEST_DURATION=20
// the command line parameter values will override the default values supplied to the getProperty method

package simulations

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

class RuntimeParameters extends Simulation {

  private def getProperty(propertyName:String, propertyDefaultValue:String) = {
    Option(System.getenv(propertyName))
      .orElse(Option(System.getProperty(propertyName)))
      .getOrElse(propertyDefaultValue)
  }

  def userCount:Int = getProperty("USERS","5").toInt
  def rampDuration:Int = getProperty("RAMP_DURATION","10").toInt
  def testDuration:Int = getProperty("TEST_DURATION","30").toInt

  before {
    println(s"Running test with ${userCount} users.")
    println(s"Ramping users over ${rampDuration} seconds.")
    println(s"Total test duration ${testDuration} seconds.")
  }

  // configuration
  val httpConf = http.baseUrl("http://localhost:8080/app/")
    .header("Accept", "application/json")

  // scenario
  val scn = scenario("demo of runtime parameters")
    .forever() {
      exec(getAllVideoGames())
    }

  setUp(
    scn.inject(
      nothingFor(5 seconds),
      rampUsers(userCount) during(rampDuration second)
    )
  ).protocols(httpConf).maxDuration(testDuration seconds)


  // functional decomposition - these could go in a separate class really
  def getAllVideoGames() = {
    exec(http("Get all video games")
      .get("videogames")
      .check(status.is(200))
    )
  }
}
