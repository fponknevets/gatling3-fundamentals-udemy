package simulations

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import scala.util.Random

import io.gatling.core.Predef._
import io.gatling.http.Predef._

class CustomFeederComplex extends Simulation{

  // configuration
  val httpConf = http.baseUrl("http://localhost:8080/app/")
    .header("Accept", "application/json")

  var idNumbers = (120 to 130).iterator
  val rnd = new Random()
  val now = LocalDate.now()
  val pattern = DateTimeFormatter.ofPattern("yyy-MM-dd")

  def getRandomString(length: Int) = {
    rnd.alphanumeric.filter(_.isLetter).take(length).mkString
  }

  def getRandomDate(startDate: LocalDate, random: Random): String = {
    startDate.minusDays(random.nextInt(30)).format(pattern)
  }

  val randomGameFeeder = Iterator.continually(
    Map(
      "gameId" -> idNumbers.next(),
      "name" -> ("Game-" + getRandomString(5)),
      "releaseDate" -> getRandomDate(now, rnd),
      "reviewScore" -> rnd.nextInt(100),
      "category" -> ("Category-" + getRandomString(6)),
      "rating" -> ("Rating-" + getRandomString(4))
    )
  )

  def postNewGameWithStringBody() = {
    repeat(5){
      feed(randomGameFeeder)
        .exec(http("Post New Game")
        .post("videogames/")
        .body(StringBody( "{" +
          "\n\t\"id\": ${gameId}," +
          "\n\t\"name\": \"${name}\"," +
          "\n\t\"releaseDate\": \"${releaseDate}\"," +
          "\n\t\"reviewScore\": ${reviewScore}," +
          "\n\t\"category\": \"${category}\"," +
          "\n\t\"rating\": \"${rating}\"\n}")
        ).asJson
        .check(status.is(200)))
        .pause(1)
    }
  }

  def postNewGameWithTemplateFile() = {
    repeat(5){
      feed(randomGameFeeder)
        .exec(http("Post New Game")
          .post("videogames/")
          .body(ElFileBody("templates/Game.json")).asJson
          .check(status.is(200)))
        .pause(1)
    }
  }

  // specify scenario using functional decomposition and code reuse
  val scn = scenario("Post New Games")
    .exec(postNewGameWithTemplateFile())

  setUp(
    scn.inject(atOnceUsers(1))
  ).protocols(httpConf)
}
