package simulations

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

class RampLoadForFixedTime extends Simulation {

  // configuration
  val httpConf = http.baseUrl("http://localhost:8080/app/")
    .header("Accept", "application/json")

  def getAllVideoGames() = {
    exec(
      http("get all video games")
        .get("videogames")
        .check(status.is(200))
    )
  }

  def getSpecificlVideoGame() = {
    exec(
      http("get specific video game")
        .get("videogames/2")
        .check(status.is(200))
    )
  }

  val scn = scenario("basic load simulation")
    .forever() {
      exec(getAllVideoGames())
        .pause(5)
        .exec(getSpecificlVideoGame())
        .pause(5)
        .exec(getAllVideoGames())
    }

  setUp(
    scn.inject(
      nothingFor(5 seconds),
      atOnceUsers(5),
      rampUsers(55) during(25 seconds)
    ).protocols(httpConf.inferHtmlResources()) // infer makes calls for other html resources on the page
  ).maxDuration(60 seconds)
}
