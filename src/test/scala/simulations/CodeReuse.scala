package simulations

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration.DurationInt

class CodeReuse extends Simulation{

  // configuration
  val httpConf = http.baseUrl("http://localhost:8080/app/")
    .header("Accept", "application/json")
    // .proxy(Proxy("localhost",8888))

  // specify scenario
  val scn_withoutCodeReuse = scenario("Video Game DB - 3 calls with pause")

    .exec(http("Get all video games - 1st call")
    .get("videogames")
    .check(status.is(200)))

    .exec(http("Get specific game")
    .get("videogames/3")
    .check(status.is(200)))

    .exec(http("Get all video games - 1st call")
    .get("videogames")
    .check(status.is(200)))

  // functional decomposition - these could go in a separate class really
  def getAllVideoGames() = {
    exec(http("Get all video games - 1st call")
      .get("videogames")
      .check(status.is(200))
    )
  }

  // functional decomposition - these could go in a separate class really
  def getVideoGameWithId(gameId : String) = {
    exec(http("Get all video games - 1st call")
      .get("videogames/" + gameId)
      .check(status.is(200))
    )
  }

  // specify scenario using functional decomposition and code reuse
  val scn_withCodeReuse = scenario("Video Game DB - 3 calls with pause")
    .exec(getAllVideoGames())
    .exec(getVideoGameWithId("3"))
    .exec(getAllVideoGames())

  setUp(
    scn_withCodeReuse.inject(atOnceUsers(1))
  ).protocols(httpConf)
}
